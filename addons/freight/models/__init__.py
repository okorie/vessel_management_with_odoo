# -*- coding: utf-8 -*-
# Part of AYIRA MARITIME. See LICENSE file for full copyright and licensing details.

from . import freight_vessel
from . import freight_vessel_cost
from . import freight_vessel_model
